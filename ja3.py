import hashlib
import csv
from datetime import datetime

TLS_VERSIONS = {
    769: "TLS 1.0",
    770: "TLS 1.1",
    771: "TLS 1.2",
    772: "TLS 1.3"
}

GREEN = "\033[92m"
RESET = "\033[0m"

def extract_tls_version(ja3_fingerprint):
    tls_version_code = int(ja3_fingerprint.split(',')[0])
    tls_version = TLS_VERSIONS.get(tls_version_code, "Unknown")
    return tls_version

def extract_ciphersuites(ja3_fingerprint):
    ciphersuites = ja3_fingerprint.split(',')[1].strip().replace(" ", "-")
    return ciphersuites.replace("--", ", ").replace("-", ", ")

def extract_extensions(ja3_fingerprint):
    extensions = ja3_fingerprint.split(',')[2].strip()
    if extensions.startswith("-"):
        extensions = extensions[1:]
    return extensions.replace(" ", "-").replace("--", ", ").replace("-", ", ")

def extract_elliptic_curves(ja3_fingerprint):
    elliptic_curves = ja3_fingerprint.split(',')[3].strip().replace(" ", "-")
    return elliptic_curves.replace("--", ", ").replace("-", ", ")

def extract_elliptic_curve_formats(ja3_fingerprint):
    elliptic_curve_formats = ja3_fingerprint.split(',')[4].strip().replace(" ", "-")
    return elliptic_curve_formats.replace("--", ", ").replace("-", ", ")

def validate_ja3_fingerprint(ja3_fingerprint):
    fingerprint_parts = ja3_fingerprint.split(',')
    return len(fingerprint_parts) == 5

def ja3_to_f5_hash(ja3):
    ja3_parts = ja3.split(",")
    if len(ja3_parts) < 4:
        return None
    ciphs = []
    if not ja3_parts[1] == "":
        for ciph in ja3_parts[1].split(" "):
            if int(ciph) < 0:
                ciphs.append(str(65536 + int(ciph)))
            else:
                ciphs.append(ciph)
    exts = []
    if not ja3_parts[2] == "":
        for ext in ja3_parts[2].split(" "):
            if int(ext) < 0:
                exts.append(str(65536 + int(ext)))
            else:
                exts.append(ext)
    ec_curves = []
    if not ja3_parts[3] == "":
        for ec_curve in ja3_parts[3].split(" "):
            ec_curves.append(ec_curve)
    ec_pt_fmts = []
    if not ja3_parts[4] == "":
        for ec_pt_fmt in ja3_parts[4].split(" "):
            ec_pt_fmts.append(ec_pt_fmt)
    return "{},{},{},{},{}".format(
        ja3_parts[0],
        "-".join(ciphs),
        "-".join(exts),
        "-".join(ec_curves),
        "-".join(ec_pt_fmts),
    )

def main():
    print("JA3 Fingerprint - MD5 Hash Converter")
    print("1. Convert JA3 Fingerprint to MD5 Hash")
    print("2. Extract and explain JA3 components")
    choice = input("Enter your choice (1/2): ")

    if choice == '1':
        ja3_fingerprint = input("Enter the JA3 fingerprint: ")

        if validate_ja3_fingerprint(ja3_fingerprint):
            f5_ja3 = ja3_to_f5_hash(ja3_fingerprint)
            f5_hash_object = hashlib.md5(f5_ja3.encode()) if f5_ja3 else None
            f5_hashed_ja3 = f5_hash_object.hexdigest() if f5_hash_object else "N/A"

            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            with open('ja3_hashes.csv', 'a', newline='') as csvfile:
                fieldnames = ['Timestamp', 'F5 JA3 Fingerprint', 'Standard JA3 Fingerprint', 'F5 JA3 Hash']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                if csvfile.tell() == 0:
                    writer.writeheader()

                writer.writerow({'Timestamp': timestamp,
                                 'Standard JA3 Fingerprint': f5_ja3,
                                 'F5 JA3 Fingerprint': ja3_fingerprint,
                                 'F5 JA3 Hash': f5_hashed_ja3})

            print(GREEN + "Standard JA3 Fingerprint:" + RESET, f5_ja3)
            print(GREEN + "F5 JA3 Fingerprint:" + RESET, ja3_fingerprint)
            print(GREEN + "F5 JA3 Hash:" + RESET, f5_hashed_ja3)
            print(GREEN + "Results appended to ja3_hashes.csv" + RESET)
        else:
            print("Invalid JA3 String format.")
    elif choice == '2':
        ja3_fingerprint = input("Enter the JA3 fingerprint: ")

        if validate_ja3_fingerprint(ja3_fingerprint):
            try:
                tls_version = extract_tls_version(ja3_fingerprint)
                ciphersuites = extract_ciphersuites(ja3_fingerprint)
                extensions = extract_extensions(ja3_fingerprint)
                elliptic_curves = extract_elliptic_curves(ja3_fingerprint)
                curve_formats = extract_elliptic_curve_formats(ja3_fingerprint)

                timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                with open('ja3_components.csv', 'a', newline='') as csvfile:
                    fieldnames = ['Timestamp', 'F5 JA3 Fingerprint', 'TLS Version', 'Ciphersuites', 'Extensions', 'Elliptic Curves', 'Elliptic Curve Formats']
                    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                    if csvfile.tell() == 0:
                        writer.writeheader()

                    writer.writerow({'Timestamp': timestamp,
                                     'F5 JA3 Fingerprint': ja3_fingerprint,
                                     'TLS Version': tls_version,
                                     'Ciphersuites': ciphersuites,
                                     'Extensions': extensions,
                                     'Elliptic Curves': elliptic_curves,
                                     'Elliptic Curve Formats': curve_formats})

                print(GREEN + "Results appended to ja3_components.csv" + RESET)
                print(GREEN + "F5 JA3 Fingerprint:" + RESET, ja3_fingerprint)
                print(GREEN + "TLS Version:" + RESET, tls_version)
                print(GREEN + "Detected ciphersuites:" + RESET, "Ciphersuites:", ciphersuites)
                print(GREEN + "Detected extensions:" + RESET, "Extensions:", extensions)
                print(GREEN + "Detected elliptic curves:" + RESET, "Elliptic Curves:", elliptic_curves)
                print(GREEN + "Detected elliptic curve formats:" + RESET, "Elliptic Curve Formats:", curve_formats)
            except ValueError:
                print("Invalid JA3 String format.")
        else:
            print("Invalid JA3 String format.")
    else:
        print("Invalid choice. Please select either 1 or 2.")

if __name__ == "__main__":
    main()